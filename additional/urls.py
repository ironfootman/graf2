# coding: utf-8
from django.conf.urls import patterns, url

from additional.views import *

urlpatterns = patterns('',
                       url(r'^filters/', filters, name='filters'),
                       url(r'^get_pictures$', getPictures),
                       url(r'^$', AboutView.as_view(), name='adbout'),
                       url(r'^create/$', CreateView.as_view(), name='create'),
                       url(r'^create_post/$', CreatePost.as_view(), name='create_post'),
                       )
