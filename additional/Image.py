import json
import cv2
from additional.const import FILTERS, MEDIANBLUR, THRESHOLD
from itertools import count

def medianblur(func):
    def func_wrapper(self):
        if self.filters[MEDIANBLUR][0] == 'true':
            self.img = cv2.medianBlur(self.img,
                                      json.loads(self.filters[MEDIANBLUR][1]))
            return func(self)
        else:
            return func(self)
        return func(self)
    return func_wrapper


def threshold(func):
    def func_wrapper(self):
        if self.filters[THRESHOLD][0] == 'true':
            attribut = json.loads(self.filters[THRESHOLD][1]) if self.filters[THRESHOLD][1] else 0
            _, self.img = cv2.threshold(self.img, attribut, 255, cv2.THRESH_BINARY)
            return func(self)
        else:
            return func(self)

    return func_wrapper


class CImage(object):
    _ids = count(0)

    def __init__(self, request, img):
        self.request = request.POST
        self.img = img
        self.id = self._ids.next()

    @property
    def filters(self):
        filters = {}
        for filter in FILTERS:
            filters[filter] = (self.request.get(filter),
                               self.request.get('%s_attribut' % filter))
        return filters

    @medianblur
    @threshold
    def make_picture(self):
        print 'make_picture'
        print '-' * 100
        print self._ids
        print '-' * 100
        return self.img
