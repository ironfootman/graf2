import base64

import cv2
import numpy as np
from django.http.response import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.views.generic.edit import FormView
from django.views.generic import TemplateView

from additional.Image import CImage
from django.core.cache import cache





class AboutView(TemplateView):
    template_name = "additional/index.html"


@csrf_exempt
def filters(request):
    print request.POST
    imgdata = cache.get('image')
    # image = cv2.imdecode(np.frombuffer(imgdata, np.uint8), 0)
    # image = cv2.imdecode(np.frombuffer(imgdata, np.uint8), 0,  iscolor=CV_LOAD_IMAGE_COLOR)
    # img_array = np.asarray(bytearray(imgdata), dtype=np.uint8)
    im = cv2.imdecode( np.asarray(bytearray(imgdata), dtype=np.uint8), 1)
    # cv2.imwrite(r'/home/envy/something.jpg', im)
    im = CImage(request, im)
    image = im.make_picture()
    encode_param = [int(cv2.IMWRITE_JPEG_QUALITY), 90]
    result, imgencode = cv2.imencode('.jpg', image, encode_param)
    content = base64.b64encode(imgencode)
    return HttpResponse('data:image/jpg;base64,' + content)

@csrf_exempt
def getPictures(request):
    if request.POST.get('dataImg'):
        res = request.POST.get('dataImg').replace('data:image/jpeg;base64,', '')
        imgdata = base64.b64decode(res)
        cache.set('image', imgdata)
    return HttpResponse('Hello image!')


class CreateView(FormView):
    template_name = 'additional/create_post.html'


class CreatePost(CreateView):
    template_name = 'alpha/create_post.html'
    success_url = '/post'
