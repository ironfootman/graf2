

function sendPicture(dataUrl) {

    $.ajax({
        url: '/photo_page/get_pictures',
        method: "POST",
        data: { dataImg : dataUrl }
    }).done(function( response ) {
        console.log('load picture');
    }).fail(function(error) {
        console.log('error load!');
    });
}

var openFile = function(event) {
    var fileTypes = ['jpg', 'gif',]
    var input = event.target;
    var extension = input.files[0].name.split('.').pop().toLowerCase()
    console.log(extension)
    isSuccess = fileTypes.indexOf(extension) > -1;
    if (isSuccess) {
        var reader = new FileReader();
        reader.onload = function () {
            var dataURL = reader.result;
            $('.showPicture').attr('src', dataURL);
            console.log(dataURL)
            sendPicture(dataURL)
        };
        reader.readAsDataURL(input.files[0]);

        $('#res_pic').hide();
        $('.btn').addClass('off').find('[type=checkbox]').prop('checked', false);



    }
    else {
        alert('wrong type')
    }
}

$('.btn-toggle').click(function() {
    $(this).find('.btn').toggleClass('active');

    if ($(this).find('.btn-primary').size()>0) {
    	$(this).find('.btn').toggleClass('btn-primary');
    }
    if ($(this).find('.btn-danger').size()>0) {
    	$(this).find('.btn').toggleClass('btn-danger');
    }
    if ($(this).find('.btn-success').size()>0) {
    	$(this).find('.btn').toggleClass('btn-success');
    }
    if ($(this).find('.btn-info').size()>0) {
    	$(this).find('.btn').toggleClass('btn-info');
    }

    $(this).find('.btn').toggleClass('btn-default');

});

$(document).ready(function() {
    $("#medianblur_attribut").keydown(function (e) {
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
            (e.keyCode == 65 && e.ctrlKey === true) ||
            (e.keyCode == 67 && e.ctrlKey === true) ||
            (e.keyCode == 88 && e.ctrlKey === true) ||
            (e.keyCode >= 35 && e.keyCode <= 39)) {
                 return;
        }
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
});

$(document).on('change', '.btn-file :file', function() {
    var input = $(this),
        numFiles = input.get(0).files ? input.get(0).files.length : 1,
        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
    input.trigger('fileselect', [numFiles, label]);
});

$(document).ready( function() {
    $('.btn-file :file').on('fileselect', function(event, numFiles, label) {
        console.log(numFiles);
        console.log(label);
    });
});

function hello_world() {
    var data = {
        'threshold': $('#medianblur').prop('checked'),
        'threshold_attribut': $('#medianblur_attribut').prop('value'),
        'medianblur': $('#threshold').is(':checked'),
        'medianblur_attribut': $('#threshold_attribut').prop('value')}


    if ($('#medianblur').is(':checked')) {
        $('#medianblur_attribut').prop('disabled', true)
    } else {
        $('#medianblur_attribut').prop('disabled', false)
    }
    sendFilterValue(data)
};

function sendFilterValue(object) {
    if ($('.showPicture').attr('src')) {
        console.log('here!')
        var f_filter = $('#medianblur').is(':checked')
        if (!object) {
            console.log('if')
            var object = {'threshold': f_filter,
                'threshold_attribut': $('#medianblur_attribut').prop('value'),
                'medianblur_attribut': $('#threshold_attribut').prop('value'),
                'medianblur': $('#threshold').is(':checked')}
        } else {
            console.log('else')
            object['medianblur'] = f_filter
        }
        $.ajax({
            //url: "{% url 'photo_page:filters' %}",
            url: "/photo_page/filters/",
            method: "POST",
            data: object
        }).done(function (response) {
            console.log('RESPONSE')
            console.log(response);
            $('#res_pic').attr('src', response);
            $('#res_pic').show();
//        document.getElementById('res_pic').setAttribute( 'src', response)
        }).fail(function (error) {
            console.log('error load!');
        });
    } else {
        alert('Upload the picture plz!')
        $('.btn').addClass('off').find('[type=checkbox]').prop('checked', false);
    }
}

$().ready(function() {
    var $scrollingDiv = $("#scrollingDiv");

    $(window).scroll(function(){
        $scrollingDiv
            .stop()
            .animate({"marginTop": ($(window).scrollTop() )}, "slow" );
    });
});


$.ajaxSetup({
     beforeSend: function(xhr, settings) {
         function getCookie(name) {
             var cookieValue = null;
             if (document.cookie && document.cookie != '') {
                 var cookies = document.cookie.split(';');
                 for (var i = 0; i < cookies.length; i++) {
                     var cookie = jQuery.trim(cookies[i]);
                 if (cookie.substring(0, name.length + 1) == (name + '=')) {
                     cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                     break;
                 }
             }
         }
         return cookieValue;
         }
         if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
             // Only send the token to relative URLs i.e. locally.
             xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
         }
     }
});
//-------------------------------------------------------------------__

function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}
$(document).ajaxSend(function(event, xhr, settings) {
    function getCookie(name) {
        var cookieValue = null;
        if (document.cookie && document.cookie != '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) == (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }
    function sameOrigin(url) {
        var host = document.location.host; // host + port
        var protocol = document.location.protocol;
        var sr_origin = '//' + host;
        var origin = protocol + sr_origin;
        return (url == origin || url.slice(0, origin.length + 1) == origin + '/') ||
            (url == sr_origin || url.slice(0, sr_origin.length + 1) == sr_origin + '/') ||
            !(/^(\/\/|http:|https:).*/.test(url));
    }
    function safeMethod(method) {
        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    }

    if (!safeMethod(settings.type) && sameOrigin(settings.url)) {
        xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
    }
});