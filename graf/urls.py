from django.conf.urls import include, url
from django.contrib import admin
from additional.views import AboutView
urlpatterns = [
    # Examples:
    # url(r'^$', 'graf.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^photo_page/', include('additional.urls', namespace='photo_page')),
    url(r'^about$', AboutView.as_view(), name='adbout'),
    # url(r'^login/$', 'additional.views.login_view', name='login'),
    # url(r'^logout/$', 'additional.views.logout_view', name='logout'),
]
